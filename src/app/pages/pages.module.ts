import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from '../app-routing.module';

import { InicioComponent } from './inicio/inicio.component';
import { NosotrosComponent } from './nosotros/nosotros.component';
import { ContactoComponent } from './contacto/contacto.component';
import { SharedModule } from '../shared/shared.module';
import { MDBBootstrapModule } from 'angular-bootstrap-md';



@NgModule({
  declarations: [InicioComponent, NosotrosComponent, ContactoComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    SharedModule,
    MDBBootstrapModule
  ],
  exports: [
    InicioComponent,
    NosotrosComponent,
    ContactoComponent
  ]
})
export class PagesModule { }
