import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from '../app-routing.module';

import { CoutaComponent } from './couta/couta.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';



@NgModule({
  declarations: [CoutaComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    MDBBootstrapModule
  ],
  exports: [
    CoutaComponent
  ]
})
export class SharedModule { }
